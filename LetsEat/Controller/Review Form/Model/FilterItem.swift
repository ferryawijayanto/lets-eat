//
//  FilterItem.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 19/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

class FilterItem: NSObject {
    let filter: String
    let name: String
    
    init(dict: [String: AnyObject]) {
        filter = dict["filter"] as! String
        name = dict["name"] as! String
    }
}
