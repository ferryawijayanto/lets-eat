//
//  MapStore.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 12/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation
import MapKit

class MapStore: DataManager {
    
    fileprivate var items = [RestaurantItem]()
    
    // Get annotation from restaurant items
    var annotation: [RestaurantItem] {
        return items
    }
    
    func fetch(completion: (_ annotations: [RestaurantItem]) -> ()) {
        let restaurantAPI = RestaurantStore()
        restaurantAPI.fetch(by: "Boston") { (items) in
            self.items = items
            completion(items)
        }
    }
    
    func currentRegion(latDelta: CLLocationDegrees, lonDelta: CLLocationDegrees) -> MKCoordinateRegion {
        // Get coordinate from first items array
        guard let item = items.first else { return MKCoordinateRegion() }
        
        // Zoom in map
        let span = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta)
        
        return MKCoordinateRegion(center: item.coordinate, span: span)
    }
}
