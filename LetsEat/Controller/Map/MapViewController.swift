//
//  MapViewController.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 12/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    let mapStore = MapStore()
    var selectedRestaurant: RestaurantItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialize()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case Segue.showDetail.rawValue:
            showRestaurantDetail(segue: segue)
        default:
            print("segue not added")
        }
    }
}

// MARK: Private extension
private extension MapViewController {
    func initialize() {
        mapView.delegate = self
        mapStore.fetch(completion: { addMap($0) })
    }
    
    func addMap(_ annotations: [RestaurantItem]) {
        mapView.setRegion(mapStore.currentRegion(latDelta: 0.5, lonDelta: 0.5), animated: true)
        mapView.addAnnotations(mapStore.annotation)
    }
    
    func showRestaurantDetail(segue: UIStoryboardSegue) {
        if
            let restaurantDetailVC = segue.destination as? RestaurantDetailViewController,
            let restaurant = selectedRestaurant {
            restaurantDetailVC.selectedRestaurant = restaurant
        }
    }
}

// MARK: MKMapViewDelegate
extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let annotation = mapView.selectedAnnotations.first else { return }
        selectedRestaurant = annotation as? RestaurantItem
        
        self.performSegue(withIdentifier: Segue.showDetail.rawValue, sender: self)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "custompin"
        
        // To ensure the annotation is not a user location
        guard !annotation.isKind(of: MKUserLocation.self) else { return nil }
        
        var annotationView: MKAnnotationView?
        
        // Check if there is any annotation already created that we can reuse
        // If so, set a variable we just created. Otherwise called out a button with detail accessory
        if let customAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
            annotationView = customAnnotationView
            annotationView?.annotation = annotation
        } else {
            let av = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            av.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            annotationView = av
        }
        
        if let annotationView = annotationView {
            // Make sure custom annotation is being called out
            annotationView.canShowCallout = true
            
            // Set our custom image
            annotationView.image = #imageLiteral(resourceName: "custom-annotation.pdf")
        }
        
        return annotationView
    }
}
