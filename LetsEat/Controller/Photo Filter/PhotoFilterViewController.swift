//
//  PhotoFilterViewController.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 11/07/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation

class PhotoFilterViewController: UIViewController {
    
    var image: UIImage?
    var thumbnail: UIImage?
    let store = FilterStore()
    var selectedRestaurantID: Int?
    var data = [FilterItem]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgExample: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        initialize()
        // Do any additional setup after loading the view.
    }
    
    func showApplyFilter() {
        store.fetch(completion: { items in
            if data.count > 0 { data.removeAll() }
            data = items
            if let image = self.image {
                imgExample.image = image
                collectionView.reloadData()
            }
        })
    }
    
    func filterItem(at indexPath: IndexPath) -> FilterItem {
        return data[indexPath.item]
    }
    
    @IBAction func onPhotoTapped(_ sender: Any) {
        checkSource()
    }
}

// MARK:- Private Extension
private extension PhotoFilterViewController {
    func initialize() {
        requestAccess()
        setupCollectionView()
        checkSource()
    }
    
    func requestAccess() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
            if granted {}
        })
    }
    
    func checkSource() {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .authorized:
            showCameraUserInterface()
        case .restricted, .denied:
            break
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: cameraMediaType, completionHandler: { granted in
                if granted {
                    self.showCameraUserInterface()
                }
            })
        }
    }
}

// MARK:- Extension UICollectionViewDataSource
extension PhotoFilterViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterCell", for: indexPath) as! FilterCell
        
        let item = self.data[indexPath.row]
        if let img = self.thumbnail {
            cell.set(image: img, item: item)
        }
        
        return cell
    }
}

// MARK:- Extension UICollectionViewDelegate
extension PhotoFilterViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.data[indexPath.row]
        filterSelected(item: item)
    }
    
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 7
        
        collectionView.collectionViewLayout = layout
        collectionView.delegate = self
        collectionView.dataSource = self
    }
}

// MARK:- Extension UICollectionViewDelegateFlowLayout
extension PhotoFilterViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenRect = collectionView.frame.size.height
        let screenHeight = screenRect - 14
        
        return CGSize(width: 150, height: screenHeight)
    }
}

// MARK:- Extension UIImagePickerControllerDelegate
extension PhotoFilterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // Get image picker from info
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.editedImage] as? UIImage
        if let img = image {
            self.thumbnail = generate(image: img, ratio: CGFloat(752))
            self.image = generate(image: img, ratio: CGFloat(102))
        }
        picker.dismiss(animated: true, completion: { self.showApplyFilter() })
    }
    
    // Show camera interface and camera control
    func showCameraUserInterface() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        #if targetEnvironment(simulator)
        imagePicker.sourceType = .photoLibrary
        #else
        imagePicker.sourceType = .camera
        imagePicker.showsCameraControls = true
        #endif
        imagePicker.mediaTypes = [kUTTypeImage as String]
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // Reduce size and crop them into smaller size of image from picking photo in library or camera.
    func generate(image: UIImage, ratio: CGFloat) -> UIImage {
        // Get size of an image
        let size = image.size
        // Get reference of an image size to crop
        var croppedSize: CGSize?
        // Set default position of an image
        var offsetX: CGFloat = 0.0
        var offsetY:CGFloat = 0.0
        
        if size.width > size.height {
            offsetX = (size.height - size.width) / 2
            croppedSize = CGSize(width: size.width, height: size.height)
        } else {
            offsetY = (size.width - size.height) / 2
            croppedSize = CGSize(width: size.width, height: size.height)
        }
        
        guard let cropped = croppedSize, let cgImage = image.cgImage else { return UIImage() }
        
        // Clipped image so it's not out of bounds
        let clippedRect = CGRect(x: offsetX * -1, y: offsetY * -1, width: cropped.width, height: cropped.height)
        let imgRef = cgImage.cropping(to: clippedRect)
        let rect = CGRect(x: 0.0, y: 0.0, width: ratio, height: ratio)
        
        UIGraphicsBeginImageContext(rect.size)
        
        if let ref = imgRef {
            UIImage(cgImage: ref).draw(in: rect)
        }
        
        let thumbnail = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        guard let thumb = thumbnail else { return UIImage() }
        return thumb
    }
}

// MARK:- Extension ImageFiltering protocol
extension PhotoFilterViewController: ImageFiltering, ImageFilteringDelegate {
    func filterSelected(item: FilterItem) {
        let filteredImg = image
        if let img = filteredImg {
            if item.filter != "None" {
                imgExample.image = self.apply(filter: item.filter, originalImage: img)
            } else {
                imgExample.image = img
            }
        }
    }
}
