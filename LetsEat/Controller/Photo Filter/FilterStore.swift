//
//  FilterStore.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 24/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

class FilterStore: DataManager {
    func fetch(completion:(_ items: [FilterItem]) -> Swift.Void) {
        var items = [FilterItem]()
        for data in load(file: "FilterData") {
            items.append(FilterItem(dict: data))
        }
        completion(items)
    }
}
