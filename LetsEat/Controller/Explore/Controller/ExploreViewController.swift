//
//  ExploreViewController.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 11/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class ExploreViewController: UIViewController, UICollectionViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let store = ExploreStore()
    var selectedCity: LocationItem?
    var headerView: ExploreHeaderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case Segue.locationList.rawValue:
            showLocationList(segue: segue)
        case Segue.restaurantList.rawValue:
            showRestaurantListing(segue: segue)
        default:
            print("Segue not added")
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == Segue.restaurantList.rawValue {
            guard selectedCity != nil else {
                showAlert()
                return false
            }
            return true
        }
        return true
    }
}

// MARK: Private Extensions
private extension ExploreViewController {
    
    func initialize() {
        store.fetch()
    }
    
    func showLocationList(segue: UIStoryboardSegue) {
        guard
            let navController = segue.destination as? UINavigationController,
            let locationVC = navController.topViewController as? LocationViewController
        else { return }
        
        guard let city = selectedCity else { return }
        locationVC.selectedCity = city
    }
    
    func showRestaurantListing(segue: UIStoryboardSegue) {
        if
            let restaurantVC = segue.destination as? RestaurantViewController,
            let city = selectedCity,
            let index = collectionView.indexPathsForSelectedItems?.first,
            let type = store.explore(at: index).name {
            restaurantVC.selectedCity = city
            restaurantVC.selectedType = type
        }
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Location Needed", message: "Please select a location", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        
        alert.addAction(okAction)
        present(alert, animated: true)
    }
    
    @IBAction func unwindLocationCancel(segue: UIStoryboardSegue) {}
    @IBAction func unwindLocationDone(segue: UIStoryboardSegue) {
        if let locationVC = segue.source as? LocationViewController {
            selectedCity = locationVC.selectedCity
            
            if let location = selectedCity {
                headerView.lblLocation.text = location.full
            }
        }
    }
}

// MARK: UICollectionViewDataSource
extension ExploreViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! ExploreHeaderView
        
        headerView = header
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "exploreCell", for: indexPath) as! ExploreCell
        
        let item = store.explore(at: indexPath)
        
        if
            let name = item.name,
            let image = item.image {
            cell.imgExplore.image = UIImage(named: image)
            cell.lblName.text = name
        }
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return store.numberOfItems()
    }
}
