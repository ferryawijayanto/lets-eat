//
//  ExploreCell.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 11/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class ExploreCell: UICollectionViewCell {
    
    @IBOutlet weak var imgExplore: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}
