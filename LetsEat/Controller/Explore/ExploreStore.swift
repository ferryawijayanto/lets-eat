//
//  ExploreStore.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 11/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

class ExploreStore: DataManager {
    
    fileprivate var items = [ExploreItem]()
    
    func fetch() {
        for data in load(file: "ExploreData") {
            items.append(ExploreItem(dict: data))
        }
    }
    
    func numberOfItems() -> Int {
        return items.count
    }
    
    func explore(at index: IndexPath) -> ExploreItem {
        return items[index.item]
    }
}
