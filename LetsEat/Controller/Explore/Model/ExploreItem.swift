//
//  ExploreItem.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 11/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

struct ExploreItem {
    let name: String?
    let image: String?
}

extension ExploreItem {
    init(dict: [String: AnyObject]) {
        self.name = dict["name"] as? String
        self.image = dict["image"] as? String
    }
}
