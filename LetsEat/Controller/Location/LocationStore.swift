//
//  LocationStore.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 11/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

class LocationStore: DataManager {
    
    private var locations = [LocationItem]()
    
    init() {
        fetch()
    }
    
    func fetch() {
        for location in load(file: "Locations") {
            locations.append(LocationItem(dict: location))
        }
    }
    
    func numberOfItems() -> Int {
        return locations.count
    }
    
    func locationItem(at index: IndexPath) -> LocationItem {
        return locations[index.item]
    }
    
    func findLocation(by name: String) -> (isFound: Bool, position: Int) {
//        guard let index = locations.firstIndex(where: $0 == name) else { return (isFound: false, position: 0) }
        guard let index = locations.firstIndex (where: {$0.city == name} ) else { return (isFound: false, position: 0) }
        
        return (isFound: true, position: index)
    }
}
