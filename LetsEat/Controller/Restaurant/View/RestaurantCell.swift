//
//  RestaurantCell.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 14/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class RestaurantCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCuisine: UILabel!
    @IBOutlet weak var imgRestaurant: UIImageView!
}
