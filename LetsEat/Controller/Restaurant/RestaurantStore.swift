//
//  RestaurantStore.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 14/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

class RestaurantStore {
    
    private var items = [RestaurantItem]()
    
    func fetch(by location: String, with filter: String = "All", completion: (_ items: [RestaurantItem]) -> ()) {
//        var restaurants = [RestaurantItem]()
        
        if let file = Bundle.main.url(forResource: location, withExtension: "json") {
            do {
                let data = try Data(contentsOf: file)
                let restaurants = try JSONDecoder().decode([RestaurantItem].self, from: data)
                
                if filter != "All" {
                    items = restaurants.filter({ $0.cuisines.contains(filter) })
                } else {
                    items = restaurants
                }
            } catch {
                print("There was an error \(error)")
            }
        }
        completion(items)
    }
    
    func numberOfItems() -> Int {
        return items.count
    }
    
    func restaurantItem(at index: IndexPath) -> RestaurantItem {
        return items[index.item]
    }
}
