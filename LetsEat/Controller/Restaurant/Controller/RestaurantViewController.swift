//
//  RestaurantViewController.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 11/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class RestaurantViewController: UIViewController, UICollectionViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var restaurantStore = RestaurantStore()
    var selectedRestaurant: RestaurantItem?
    var selectedCity: LocationItem?
    var selectedType: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let location = selectedCity?.city, let type = selectedType else { return }
        
        print("type \(type)")
        print(RestaurantAPI.loadJSON(file: location))
        
        createData()
        setupTitle()
    }
}

// MARK: Private extensions
private extension RestaurantViewController {
    
    func createData() {
        guard let city = selectedCity?.city, let filter = selectedType else { return }
        
        restaurantStore.fetch(by: city, with: filter, completion: { _ in
            if restaurantStore.numberOfItems() > 0 {
                collectionView.backgroundView = nil
            } else {
                let view = NoDataView(frame: CGRect(x: 0, y: 0, width: collectionView.frame.width, height: collectionView.frame.height))
                view.set(title: "Restaurant")
                view.set(desc: "No Restaurant")
                
                collectionView.backgroundView = view
            }
            collectionView.reloadData()
        })
    }
    
    func setupTitle() {
        navigationController?.setToolbarHidden(false, animated: true)
        
        if let city = selectedCity?.city, let state = selectedCity?.state {
            title = "\(city.uppercased()), \(state.uppercased())"
        }
        navigationController?.navigationBar.prefersLargeTitles = true
    }
}

// MARK: UICollectionViewDataSource
extension RestaurantViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "restaurantCell", for: indexPath) as! RestaurantCell
        
        let item = restaurantStore.restaurantItem(at: indexPath)
        
        if let name = item.name, let cuisine = item.subtitle, let image = item.imageURL, let url = URL(string: image) {
            cell.lblTitle.text = name
            cell.lblCuisine.text = cuisine
            
            let data = try? Data(contentsOf: url)
            
            if let imageData = data {
                DispatchQueue.main.async {
                    cell.imgRestaurant.image = UIImage(data: imageData)
                }
            }
        }
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return restaurantStore.numberOfItems()
    }
}
