//
//  RestaurantAPI.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 13/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

struct RestaurantAPI {
    
    static func loadJSON(file name: String) -> [[String: AnyObject]] {
        var items = [[String: AnyObject]]()
        
        guard
            let path = Bundle.main.path(forResource: name, ofType: "json"),
            let data = NSData(contentsOfFile: path) else { return [[:]] }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as AnyObject
            if let restaurants = json as? [[String: AnyObject]] { items = restaurants as [[String: AnyObject]]}
        } catch let jsonErr {
            print("Error serializing JSON \(jsonErr)")
            items = [[:]]
        }
        return items
    }
}
