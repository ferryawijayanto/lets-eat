//
//  Segue.swift
//  LetsEat
//
//  Created by Ferry Adi Wijayanto on 12/06/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

enum Segue: String {
    case showDetail
    case showRating
    case showReview
    case showAllReviews
    case restaurantList
    case locationList
    case showPhotoReview
}
